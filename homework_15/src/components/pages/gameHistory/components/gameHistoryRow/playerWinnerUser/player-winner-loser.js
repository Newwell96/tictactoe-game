import React from 'react';
import MyImage from "../../../../../UI components/image/image";
import './player-winner-loser.css';

const PlayerWinnerLoser = (
    {
        playerName,
        playerSide,
        playerStatus,
    }

) => {

    function choseSymbol () {
        if (playerSide === "X") {
            return "miniX";
        }
        else if (playerSide === "O") {
            return "miniZero";
        }
    }

    return (
        <div
            className="player-winner-loser"
        >
            <MyImage
                name={choseSymbol()}
                width={16}
                height={16}
            />

            {playerName}

            {playerStatus && <MyImage
                name={playerStatus}
                width={24}
                height={24}
            />}
        </div>
    );
};

export default PlayerWinnerLoser;