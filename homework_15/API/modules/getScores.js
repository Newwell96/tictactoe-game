const knex = require("../db/knex");

const getScores = async function (account) {
    try {
        const query = `
            SELECT
                CONCAT(users."surName", ' ', users."firstName", ' ', users."secondName") AS name,
                SUM (wins_cnt + loses_cnt + draws_cnt) AS totals,
                SUM (wins_cnt) AS wins,
                SUM (loses_cnt) AS loses,
                ROUND(SUM (wins_cnt::float) / SUM (wins_cnt + loses_cnt + draws_cnt) * 100)  AS percent
            FROM scores
            JOIN users ON scores.user_id = users.id
            WHERE month >= date_trunc('month', now()) - interval '6 month'
            AND users."statusActive" = true
            GROUP BY users."surName", users."firstName", users."secondName"
            HAVING SUM (wins_cnt + loses_cnt + draws_cnt) > 10
            ORDER BY SUM(wins_cnt * (CASE WHEN symbol = 'X' THEN 0.9 ELSE 1 END) - loses_cnt* (CASE WHEN symbol = 'O' THEN 1.1 ELSE 1 END) + draws_cnt * 0.25) /
                COUNT (DISTINCT month) DESC
    `;
        const result = await knex.raw(query);
        return result.rows;
    } catch (err) {
        console.error(err);
    }
}

module.exports = getScores;


