CREATE TABLE counters_x (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL REFERENCES users(id),
    symbol CHAR(1) NOT NULL,
    month TIMESTAMP NOT NULL DEFAULT date_trunc('month', now()),
    wins_cnt INTEGER NOT NULL DEFAULT 0,
    loses_cnt INTEGER NOT NULL DEFAULT 0,
    draws_cnt INTEGER NOT NULL DEFAULT 0,
    UNIQUE (user_id, symbol, month)
);
INSERT INTO counters_x(user_id, symbol, month, wins_cnt, loses_cnt, draws_cnt)
SELECT u.id AS user_id,
        'X' AS symbol,
        date_trunc('month', g."gameBegin") AS month,
        COUNT(g.id) FILTER ( WHERE"gameResult" = TRUE ) AS wins_cnt,
        COUNT(g.id) FILTER ( WHERE "gameResult" = FALSE ) AS loses_cnt,
        COUNT(g.id) FILTER ( WHERE "gameResult" IS NULL) AS draws_cnt
    FROM users AS u
        JOIN games AS g ON u.id = g."idPlayer1" OR u.id = g."idPlayer2"
    GROUP BY u.id, date_trunc('month', g."gameBegin")
    UNION
    SELECT u.id AS user_id,
        'O' AS symbol,
        date_trunc('month', g."gameBegin") AS month,
        COUNT(g.id) FILTER ( WHERE"gameResult" = FALSE ) AS wins_cnt,
        COUNT(g.id) FILTER ( WHERE "gameResult" = TRUE ) AS loses_cnt,
        COUNT(g.id) FILTER ( WHERE "gameResult" IS NULL) AS draws_cnt
FROM users AS u
    JOIN games AS g ON u.id = g."idPlayer1" OR u.id = g."idPlayer2"
GROUP BY u.id, date_trunc('month', g."gameBegin")