const usersList = require("./db_xoxo_dev_public_users.json");
const accounts = require("./db_xoxo_dev_public_accounts.json");
const gamesList = require("./db_xoxo_dev_public_games.json");

const {faker} = require('@faker-js/faker/locale/ru');

const gameResult = () => {
    return Math.random() < 0.5 ? true : (Math.random() < 0.75 ? false : null)
};

exports.seed = async function (knex) {

    try {
        await knex('games').del();
        await knex('accounts').del();
        await knex('users').del();
        await knex('scores').del();

        const users = [];

        for (let i = 0; i < 2000; i++) {
            const userGender = faker.person.sex();
            users.push({
                id: i,
                surName: faker.person.lastName(userGender),
                firstName: faker.person.firstName(userGender),
                secondName: faker.person.middleName(userGender),
                age: faker.number.int({min: 18, max: 99}),
                gender: userGender === 'male',
                statusActive: faker.datatype.boolean(),
                statusGame: faker.datatype.boolean(),
                createdAt: new Date(),
                updatedAt: new Date(),
            });
        }

        let l = 2000;

        usersList.forEach(user => {
            users.push({
                id: l,
                surName: user.surName,
                firstName: user.firstName,
                secondName: user.secondName,
                age: user.age,
                gender: user.gender,
                statusActive: user.statusActive,
                statusGame: user.statusGame,
                createdAt: new Date(),
                updatedAt: new Date(),
            });
            l++;
        });

        await knex('users').insert(users);
        await knex('accounts').insert(accounts);

        const games = [];
        const group1 = users.slice(0, 200);
        const group2 = users.slice(200, 400);
        const group3 = users.slice(400, 600);
        const group4 = users.slice(600);
        let gameID = 0;

        console.log("group1 started");

        //40 000 игр
        for (let i = 0; i < group1.length; i++) {
            for (let j = i + 1; j < group1.length; j++) {

                const date1 = faker.date.between({from: '2022-05-01T00:00:00.000Z', to: '2022-11-31T00:00:00.000Z'});
                const date2 = new Date(date1.getTime() + 60 * 60 * 1000);

                games.push({
                    id: gameID++,
                    idPlayer1: group1[i].id,
                    idPlayer2: group1[j].id,
                    gameResult: gameResult(),
                    gameBegin: date1,
                    gameEnd: date2,
                });
            }
        }

        console.log("group2 started");

        //220 000 игр
        for (let i = 0; i < group2.length; i++) {

            const dateMonth = faker.date.between({from: '2022-12-01T00:00:00.000Z', to: '2023-05-30T00:00:00.000Z'})

            for (let j = 0; j < 1000; j++) {

                const date1 = faker.date.between({from: dateMonth.setDate(1), to: dateMonth.setDate(28)});
                const date2 = new Date(date1.getTime() + 60 * 60 * 1000);

                games.push({
                    id: gameID++,
                    idPlayer1: group2[i].id,
                    idPlayer2: group4[faker.number.int({min: 0, max: group4.length - 1})].id,
                    gameResult: gameResult(),
                    gameBegin: date1,
                    gameEnd: date2,
                });
            }

            for (let k = 0; k < 100; k++) {

                const date1 = faker.date.between({from: '2022-05-01T00:00:00.000Z', to: '2023-05-28T00:00:00.000Z'});
                const date2 = new Date(date1.getTime() + 60 * 60 * 1000);

                games.push({
                    id: gameID++,
                    idPlayer1: group2[i].id,
                    idPlayer2: group4[faker.number.int({min: 0, max: group4.length - 1})].id,
                    gameResult: gameResult(),
                    gameBegin: date1,
                    gameEnd: date2,
                });
            }
        }

        console.log("group3 started");

        for (let i = 0; i < group3.length; i++) {

            const dateMonth = faker.date.between({from: '2022-12-01T00:00:00.000Z', to: '2023-05-30T00:00:00.000Z'})

            for (let j = i + 1; j < group3.length; j++) {

                let date1 = faker.date.between({from: '2022-05-01T00:00:00.000Z', to: '2023-05-31T00:00:00.000Z'});
                while (date1.getMonth() === dateMonth.getMonth()) {
                    date1 = faker.date.between({from: '2022-05-01T00:00:00.000Z', to: '2023-05-31T00:00:00.000Z'});
                }
                const date2 = new Date(date1.getTime() + 60 * 60 * 1000);

                games.push({
                    id: gameID++,
                    idPlayer1: group3[i].id,
                    idPlayer2: group3[j].id,
                    gameResult: gameResult(),
                    gameBegin: date1,
                    gameEnd: date2,
                });
            }
        }

        console.log("group4 started");

        for (let i = 0; i < group4.length; i++) {
            for (let j = 0; j < group4.length; j++) {

                if (i === j) continue;

                const date1 = faker.date.between({from: '2022-05-01T00:00:00.000Z', to: '2023-05-28T00:00:00.000Z'});
                const date2 = faker.date.between({from: '2022-05-01T00:00:00.000Z', to: '2023-05-28T00:00:00.000Z'});

                games.push({
                    id: gameID++,
                    idPlayer1: group4[i].id,
                    idPlayer2: group4[j].id,
                    gameResult: gameResult(),
                    gameBegin: date1,
                    gameEnd: date2,
                });
            }
        }
        gamesList.forEach(game => {
            games.push({
                id: gameID++,
                idPlayer1: game.idPlayer1,
                idPlayer2: game.idPlayer2,
                gameResult: game.gameResult,
                gameBegin: game.gameBegin,
                gameEnd: game.gameEnd
            });
        });

        let i = 0;
        while (i < games.length) {
            console.log(i, i + 1000)
            await knex('games').insert(games.slice(i, i + 1000));
            i += 1000;
        }

        await knex.raw(`
            INSERT INTO scores(user_id, symbol, month, wins_cnt, loses_cnt, draws_cnt)
            SELECT u.id AS user_id,
                    'X' AS symbol,
                    date_trunc('month', g."gameBegin") AS month,
                    COUNT(g.id) FILTER ( WHERE "gameResult" = TRUE ) AS wins_cnt,
                    COUNT(g.id) FILTER ( WHERE "gameResult" = FALSE ) AS loses_cnt,
                    COUNT(g.id) FILTER ( WHERE "gameResult" IS NULL) AS draws_cnt
                FROM users AS u
                    JOIN games AS g ON u.id = g."idPlayer1" OR u.id = g."idPlayer2"
                GROUP BY u.id, date_trunc('month', g."gameBegin")
                UNION
                SELECT u.id AS user_id,
                    'O' AS symbol,
                    date_trunc('month', g."gameBegin") AS month,
                    COUNT(g.id) FILTER ( WHERE"gameResult" = FALSE ) AS wins_cnt,
                    COUNT(g.id) FILTER ( WHERE "gameResult" = TRUE ) AS loses_cnt,
                    COUNT(g.id) FILTER ( WHERE "gameResult" IS NULL) AS draws_cnt
            FROM users AS u
                JOIN games AS g ON u.id = g."idPlayer1" OR u.id = g."idPlayer2"
            GROUP BY u.id, date_trunc('month', g."gameBegin")
`)
            .then(function () {
                console.log('Data inserted into counters_x table');
            });

    } catch (error) {
        console.error(error);
    }
};
