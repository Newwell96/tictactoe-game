SELECT  user_id,
    SUM (wins_cnt + loses_cnt + draws_cnt) AS total_cnt,
    SUM (wins_cnt) AS wins_cnt,
    SUM (loses_cnt) AS loses_cnt,
    SUM (wins_cnt::float) / SUM (wins_cnt + loses_cnt + draws_cnt) * 100  AS wins_prc
FROM scores
JOIN users ON scores.user_id = users.id
WHERE month >= date_trunc('month', now()) - interval '6 month'
AND users."statusActive" = true
GROUP BY user_id
HAVING SUM (wins_cnt + loses_cnt + draws_cnt) > 10
ORDER BY SUM(wins_cnt * (CASE WHEN symbol = 'X' THEN 0.9 ELSE 1 END) - loses_cnt* (CASE WHEN symbol = 'O' THEN 1.1 ELSE 1 END) + draws_cnt * 0.25) /
    COUNT (DISTINCT month) DESC
;