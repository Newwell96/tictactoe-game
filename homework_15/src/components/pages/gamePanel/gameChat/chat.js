import React, {useContext, useEffect} from 'react';
import "./chat.css"
import MyInput from "../../../UI components/input/input";
import MyButton from "../../../UI components/button/button";
import MyMessage from "./components/message-chat";
import {observer} from "mobx-react";
import {GameContext} from "../../../store/GameStore";
import gameWebSocket from "../../../API_WS/webSocket";

const MyChat = () => {

    const gameStore = useContext(GameContext);

    useEffect(() => {
            const container = document.querySelector(".msgs-container");
            container.scrollTop = container.scrollHeight;
        }, [gameStore.chat]);

    useEffect(() => {

        if (gameStore.chat.length === 0) {
            return;
        }

        const container = document.querySelector(".msgs-container");
        const upDiv = document.querySelector(".fade-up");
        const downDiv = document.querySelector(".fade-down");

        const handleScroll = () => {
            if (container.scrollTop === 0) {
                upDiv.classList.add("invisible");
            } else {
                upDiv.classList.remove("invisible");
            }
            if (container.scrollTop >= container.scrollHeight - container.clientHeight) {
                downDiv.classList.add("invisible");
            } else {
                downDiv.classList.remove("invisible");
            }
        };

        const handleWindowResize = () => {
            // Проверяем, есть ли вертикальный скроллбар
            if (container.scrollHeight <= container.clientHeight) {
                upDiv.classList.add("invisible");
                downDiv.classList.add("invisible");
            } else {
                upDiv.classList.remove("invisible");
                downDiv.classList.remove("invisible");
            }
        };

        container.addEventListener("scroll", handleScroll);
        window.addEventListener("resize", handleWindowResize);

        // Вызываем обработчик события resize сразу после подписки на событие
        handleWindowResize();

        return () => {
            container.removeEventListener("scroll", handleScroll);
            window.removeEventListener("resize", handleWindowResize);
        };
    }, [gameStore.chat.length]);

    function renderMessage(data) {
        return data.map(item => (<MyMessage
                messageSide={item.side}
                name={item.name}
                time={item.time}
                messageText={item.text}
            />))
    }

    const playerName = (gameStore?.role === "X") ? gameStore?.nameX : gameStore?.nameO;

    const sendMessage = () => {

        const textMessage = document.getElementById("message").value;
        const time = new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });

        const newMessage = {
            time: time,
            name: playerName,
            side: gameStore?.role,
            text: textMessage
        };
        const newChat = [...gameStore.chat, newMessage];
        if (textMessage) {
            gameStore.setChat(newChat) ;
            document.getElementById("message").value = "";
        }
        gameWebSocket.sendMessage(newMessage.name,newMessage.text,newChat);
    };

    return (
        <div id="chat-container">

        <div className="msgs-container">
            {gameStore.chat.length === 0 ? (
                <div className="no-messages">Сообщений еще нет</div>
            ) : (
                <>
                    <div
                        className="fade-up"
                    />
                    {renderMessage(gameStore.chat)}
                    <div className="fade-down" />
                </>
            )}
        </div>

        <div className="msg-interactive-elements">

            <MyInput
                className="text-field__input"
                placeholder="Сообщение..."
                id = "message"
            />
            <MyButton
                className="button"
                imageName="send"
                buttonID={"submit"}
                onClickFunction = {sendMessage}
            />
        </div>
    </div>);
};

export default observer(MyChat);
