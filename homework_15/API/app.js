var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const timeout = require('connect-timeout');

var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth');
var scoresRouter = require('./routes/scores');
var fieldRouter = require('./routes/gameField');
var chatRouter = require('./routes/chat');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Устанавливаем таймаут в 5 секунд для всех запросов
app.use(timeout('5s'));

app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/scores', scoresRouter);
app.use('/gameField', fieldRouter);
app.use('/chat', chatRouter);

module.exports = app;

