import playersData from "../pages/gamePanel/data/playersData.json"

const playersList = playersData.users;
const checkID = JSON.parse(localStorage.getItem("userData")).idUser;

const gameAPI = {
        getPlayersInfo: async () => {
            try {
                const params = new URLSearchParams();
                params.append('id', checkID);
                const response = await fetch("/users/info?" + params.toString(), {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json"
                    }
                });
                return await response.json();
            } catch (error) {
                console.error(error, "Произошла ошибка загрузки инфо. Повторяем запрос.");
            }
        },

    sendFieldState:  (field) => {
            localStorage.setItem("currentField", JSON.stringify(field));
    },

    getFieldState:  () => {
        try {
            return localStorage.getItem("currentField");
        } catch (error) {
            console.error(error,"Произошла ошибка загрузки поля. Повторяем запрос.");
        }
    },

    sendChatMessage: (message) => {
        localStorage.setItem("chat", JSON.stringify(message));
    },

    getChatMessage:  () => {
        try {
            return localStorage.getItem("chat");
        } catch (error) {
            console.error(error, "Произошла ошибка загрузки чата. Повторяем запрос.");
        }
    },
};

export default gameAPI;

