const WebSocket = require('ws');
const wss = new WebSocket.Server({port: 9000});

// Обработка подключения нового клиента к WebSocket серверу
wss.on('connection', function connection(ws) {
    console.log('Новое подключение');
// Список комнат
    const rooms = {};
    // Обработка сообщений от клиента
    ws.on('message', function incoming(message) {
        try {
            const jsonMessage = JSON.parse(message);
            switch (jsonMessage.action) {
                // case 'join':
                // // Добавляем игрока в комнату
                // const roomId = jsonMessage.roomId;
                // ws.join(roomId);
                // if (!rooms[roomId]) {
                //     rooms[roomId] = [ws];
                // } else {
                //     rooms[roomId].push(ws);
                // }
                // break;
                case 'move':
                    console.log(`Игрок ${jsonMessage.userId} сделал ход ${jsonMessage.row}:${jsonMessage.col}`, "--Сообщение серверу--");
                    if (!jsonMessage.cell) {
                        wss.clients.forEach(function each(client) {
                            if (client.readyState === WebSocket.OPEN) {
                                client.send(JSON.stringify({
                                    message: `Игрок ${jsonMessage.userId} сделал ход ${jsonMessage.row}:${jsonMessage.col}`,
                                    field: jsonMessage.field,
                                }));
                            }
                        });
                        break;

                    } else {
                        ws.send(
                            JSON.stringify({
                                message: "Ход невозможен"}));
                        break;
                    }

                case 'message':
                    console.log(`${jsonMessage.userId} отправил сообщение: ${jsonMessage.text}`,"--Сообщение серверу--");

                    wss.clients.forEach(function each(client) {
                        if (client.readyState === WebSocket.OPEN) {
                            client.send(JSON.stringify({
                                message: `Игрок ${jsonMessage.userId} отправил сообщение: ${jsonMessage.text}`,
                                chat: jsonMessage.chat,
                            }));
                        }
                    });
                    break;

                default:
                    ws.send(
                        JSON.stringify({
                            message: "Неизвестная команда"}));
                    break;
            }
        } catch (error) {
            console.log('Ошибка хода', error);
        }
    });

// Обработка закрытия соединения клиентом
    ws.on('close', function close() {
        console.log('Соединение закрыто');
    });
})


// // Обработка сообщений от клиента
// ws.on('message', function incoming(message) {
//     try {
//         const jsonMessage = JSON.parse(message);
//         switch (jsonMessage.action) {
//             case 'join':
//                 // Добавляем игрока в комнату
//                 const roomId = jsonMessage.roomId;
//                 ws.join(roomId);
//                 if (!rooms[roomId]) {
//                     rooms[roomId] = [ws];
//                 } else {
//                     rooms[roomId].push(ws);
//                 }
//                 break;
//             case 'message':
//                 // Отправляем сообщение всем игрокам в комнате
//                 const roomId = jsonMessage.roomId;
//                 const data = jsonMessage.data;
//                 ws.to(roomId).emit('message', data);
//                 break;
//             default:
//                 ws.send(JSON.stringify({
//                     message: "Неизвестная команда"
//                 }));
//                 break;
//         }
//     } catch (error) {
//         console.log('Ошибка хода', error);
//     }
// });
//
// // Обработка закрытия соединения клиентом
// ws.on('close', function close() {
//     console.log('Соединение закрыто');
// });
// });

