const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const getAccount = require('./accounts_checker');

const authorize = async (login, password) => {

    const data = await getAccount(login);

    if (login === data.login && await bcrypt.compare(password, data.password))
    {
        delete data.password;
        return data;
    }
    return ''
}

module.exports = authorize;
