var express = require('express');
var router = express.Router();

const authMiddleware = require("../middleware/auth")
const getInfo = require("../modules/getInfo");
const getGame = require("../modules/getGame");

router.use(authMiddleware)

router.get('/me', function (req, res, next) {
  res.send({login: "test"})
});

router.get('/info', async function (req, res, next) {
  const checkID = req.query.id;
  const playersGame = await getGame(checkID);
  const playersInfo = await getInfo(playersGame);
  res.send(playersInfo)
});

module.exports = router;
