const knex = require("../db/knex");
const getGame = require("./getGame");

class Field {
    constructor (playerID, field) {
        this.field = field;
        this.playerID = playerID;
    }

    async getGameID() {
        const playersGame = await getGame(this.playerID);
        return playersGame.id;
    }

    async getField() {
        try {
            const gameID = await this.getGameID();
            return await knex('fields')
                .select("gameField")
                .where({ idGame: gameID })
                .first()
        } catch (err) {
            console.error(err);
            throw new Error("Данные не найдены");
        }
    }

    async postField() {
        try {
            const gameID = await this.getGameID();
            await knex('fields').insert(
                {
                    "idGame": gameID,
                    "gameField": this.field,
                }
            )
        } catch (err) {
            console.error(err);
            throw new Error("Ошибка добавления данных");
        }
    }

    async putField() {
        try {
            const gameID = await this.getGameID();
            await knex('fields')
                .where({ idGame: gameID })
                .update({ gameField: this.field });
        } catch (err) {
            console.error(err);
            throw new Error("Ошибка обновления данных");
        }
    }

    async delField() {
        try {
            const gameID = await this.getGameID();
            await knex('fields')
                .where({ idGame: gameID })
                .del();
        } catch (err) {
            console.error(err);
            throw new Error("Ошибка удаления данных");
        }
    }
}

module.exports = Field;
