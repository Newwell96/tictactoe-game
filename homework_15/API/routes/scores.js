var express = require('express');
var router = express.Router();
const authMiddleware = require("../middleware/auth");
const scoresData = require ("../../src/components/pages/scores/data/scoresData");
const getScores = require("../modules/getScores");

router.use(authMiddleware)

/* GET home page. */
router.get('/', async function (req, res, next) {
    const scoresList = await getScores();
    res.status(200);
    res.send({scoresList});
});

module.exports = router;
