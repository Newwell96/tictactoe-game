var express = require('express');
const authorize = require("../modules/auth_checker");
const checkAdmin = require("../middleware/checkAdmin")
const jwt = require("jsonwebtoken");
var router = express.Router();

router.post('/', async function (req, res, next) {
    const authData = req.body;
    const login = authData.login;
    const password = authData.password;

    const sessionCookey = await authorize(login, password);
    if (sessionCookey === '') {
        res.status(400);
    } else {
        const secretKey = process.env.SECRET_KEY;
        res.cookie("sessionCookey", jwt.sign(sessionCookey, secretKey))
    }
    res.json(sessionCookey);
});

    router.get("/logout", function (req, res, next) {
        res.clearCookie('sessionCookey');
        res.send('Cookie удалена');
    });

module.exports = router;
