exports.up = function(knex) {
    return knex.schema
        .createTable('users', function (table) {
            table.integer('id').primary();
            table.string('surName').notNullable();
            table.string('firstName').notNullable();
            table.string('secondName').notNullable();
            table.integer('age').notNullable();
            table.boolean('gender').notNullable();
            table.boolean('statusActive').notNullable();
            table.boolean('statusGame').notNullable();
            table.timestamp('createdAt',{ useTz: false }).notNullable();
            table.timestamp('updatedAt',{ useTz: false }).notNullable();
        })
        .createTable("accounts", function (table) {
                table.integer('idUser');
                table.foreign('idUser').references('id').inTable('users');
                table.string('login').notNullable();
                table.string('password').notNullable();
                table.boolean('isAdmin').notNullable();
            })
        .createTable("games", function (table)
            {
                table.integer('id').primary();
                table.integer('idPlayer1').notNullable();
                table.foreign('idPlayer1').references('id').inTable('users');
                table.integer('idPlayer2').notNullable();
                table.foreign('idPlayer2').references('id').inTable('users');
                table.boolean('gameResult');
                table.timestamp('gameBegin',{ useTz: false }).notNullable();
                table.timestamp('gameEnd',{ useTz: false }).nullable();
            })
        .createTable("fields", function (table)
            {
                table.integer('idGame');
                table.foreign('idGame').references('id').inTable('games');
                table.specificType('gameField', 'text ARRAY');
            })
        .createTable("messenger", function (table)
            {
                table.integer('idGame');
                table.foreign('idGame').references('id').inTable('games');
                table.integer('idUser');
                table.foreign('idUser').references('id').inTable('users');
                table.text('message').notNullable();
                table.timestamp('createdAt',{ useTz: false }).notNullable();
            })
        .createTable("scores", function (table)
            {
                table.increments('id');
                table.integer('user_id').references('id').inTable('users').notNullable();
                table.string('symbol',1).notNullable();
                table.timestamp('month', {useTz: false});
                table.integer('wins_cnt').notNullable();
                table.integer('loses_cnt').notNullable();
                table.integer('draws_cnt').notNullable();
                table.unique(['user_id', 'symbol', 'month']);
            })
};

exports.down = function(knex) {
    return knex.schema
        .dropTable("messenger")
        .dropTable("games")
        .dropTable("fields")
        .dropTable("accounts")
        .dropTable("scores")
        .dropTable("users");
};
