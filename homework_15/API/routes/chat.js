var express = require('express');
const authMiddleware = require("../middleware/auth");
const getGame = require("../modules/getGame");
const getChat = require("../modules/getChat");
const setChat = require("../modules/setChat");
var router = express.Router();
router.use(authMiddleware)

router.get('/', async function (req, res, next) {
    const checkID = req.query.id;
    const playersGame = await getGame(checkID);
    const playersChat = await getChat(playersGame);
    res.send(playersChat)
});

router.post('/', async function (req, res, next) {
    const checkID = req.body.id;
    const playersGame = await getGame(checkID);
    const dateNow = new Date();
    const newMessage = Object.assign({}, playersGame.id, req.body.message, dateNow);
    const playersChat = await setChat(newMessage);
    res.send(playersChat)
});

module.exports = router;