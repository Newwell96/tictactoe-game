const knex = require("../db/knex");

const getChat = async function (account) {

    try {
        const gameID = account.idGame;

        return knex('messenger')
            .select("*")
            .where('idGame', gameID);

    } catch (err) {
        console.error(err);
        throw new Error("Failed to get game messages");
    }
};

module.exports = getChat;
