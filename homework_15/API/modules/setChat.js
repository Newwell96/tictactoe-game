const knex = require("../db/knex");

const setChat = async function (message) {

    try {
        await knex('messenger').insert(message);
    } catch (err) {
        console.error(err);
        throw new Error("Failed to set game messages");
    }
};

module.exports = setChat;
