var express = require('express');
const authMiddleware = require("../middleware/auth");
const Field = require("../modules/changeField");
var router = express.Router();

router.use(authMiddleware)

router.get("/", async function (req, res, next) {
    console.log("чтение данных");
    const field = new Field(req.query.id);
    const gameField = await field.getField();
    res.status(200);
    res.send(gameField.gameField);
});

router.post("/", async function (req, res, next) {
    console.log("запись данных");
    const field = new Field(req.body.id, req.body.field);
    field.postField();
    res.status(200);
    res.send("Успех!");
});

router.put("/", async function (req, res, next) {
    console.log("запись данных");
    const field = new Field(req.body.id,req.body.field);
    field.putField()
    res.status(200);
    res.send("Успех!");
});

router.delete("/", async function (req, res, next) {
    console.log("удаление данных");
    const field = new Field(req.query.id);
    field.delField()
    res.status(200);
    res.send("Успех!");
});

module.exports = router;