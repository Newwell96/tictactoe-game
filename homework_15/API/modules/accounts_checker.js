const knex = require("../db/knex");

const getAccount = function (login) {

    return knex('accounts')
        .select("*")
        .where(
            {
                login: login
            }
        )
        .first()
        .then((account) => {
            return account;
        })
        .catch((err) => {
            console.error(err);
        })
};

module.exports = getAccount;