var knexConfig = require('./knexConfig');
var knex = require('knex')(knexConfig);

module.exports = knex;
