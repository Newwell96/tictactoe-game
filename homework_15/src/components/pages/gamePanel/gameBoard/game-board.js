import React, { useContext} from 'react';
import "./game-board.css"
import MyTimer from "./components/gameTimer/timer";
import GameInfoMove from "./components/gameInfoMove/game-info-move";
import MyModalWinner from "../modalWinner/modalWinner";
import {observer} from "mobx-react";
import {GameContext} from "../../../store/GameStore";

function MyBoard() {
    const gameStore = useContext(GameContext);
    function renderGameInfoMove() {
        let side;
        let name;

        if (!gameStore.users) {
            side = "";
            name = "";
        }
        if (gameStore.xIsNext) {
            side = "X"
            name = gameStore.nameX
        }
        else {
            side = "O"
            name = gameStore.nameO
        }
        return (
            <GameInfoMove
                side={side}
                name={name}
            />
        );
    }

    return (
        <div id="game-container">

                <MyTimer/>
                <div id="game-board">
                    {gameStore.renderSquare(0)}
                    {gameStore.renderSquare(1)}
                    {gameStore.renderSquare(2)}
                    {gameStore.renderSquare(3)}
                    {gameStore.renderSquare(4)}
                    {gameStore.renderSquare(5)}
                    {gameStore.renderSquare(6)}
                    {gameStore.renderSquare(7)}
                    {gameStore.renderSquare(8)}
                </div>
                {renderGameInfoMove()}
                <MyModalWinner/>
        </div>
    );
}

export default observer(MyBoard);
