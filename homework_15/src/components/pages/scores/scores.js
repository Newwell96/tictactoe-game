import React from 'react';
import '../../../index.css';
import ScoresRow from "./components/scoresRow";

function Scores({scoresData}) {
    function renderRows(data) {
        return data.map(
            item => (
                <ScoresRow
                    name={item.name}
                    total={item.totals}
                    wins={item.wins}
                    loses={item.loses}
                    percent={item.percent}
                />
            )
        )
    }

    return (
        <div className="tables_body">
            <div id="tables_container">
                <h1 className="scores_title">Рейтинг игроков</h1>
                <table className="table">
                    <thead>
                    <tr className="score_row">
                        <th className="first_colomn">
                            ФИО
                        </th>
                        <th className="tipic_colomn">
                            Всего игр
                        </th>
                        <th className="tipic_colomn">
                            Победы
                        </th>
                        <th className="tipic_colomn">
                            Проигрыши
                        </th>
                        <th className="last_colomn">
                            Процент побед
                        </th>
                    </tr>

                    </thead>
                    <tbody>
                    {scoresData ? renderRows(scoresData[0]) : null}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Scores;