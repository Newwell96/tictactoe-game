const jwt = require("jsonwebtoken");
const authMiddleware = function (req, res, next) {
    try {
        jwt.verify(req.cookies.sessionCookey, process.env.SECRET_KEY);
    } catch(err) {
        res.status(401, "ошибка middleware");
        res.send();
        return
    }
    next();
}

module.exports = authMiddleware;

