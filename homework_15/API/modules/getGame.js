const knex = require("../db/knex");

const getGame = async function (id) {

    try {
        console.log("получение игры")
        return await knex('games')
            .select("*")
            .whereNull('gameEnd')
            .andWhere(function () {
                this.where('idPlayer1', id).orWhere('idPlayer2', id);
            })
            .first();

    } catch (err) {
        console.error(err);
        throw new Error("Failed to get game ");
    }
};

module.exports = getGame;
