import React from 'react';
import './header.css';
import {BrowserRouter as Router, Link, Route, Routes} from "react-router-dom";
import Scores from "../scores/scores";
import XOPanel from "../gamePanel/xo-panel";
import Auth from "../auth/auth";
import ActivePlayers from "../activePlayers/activePlayers";
import GameHistory from "../gameHistory/gameHistory";
import PlayerList from "../playersList/players_list";
import MyImage from "../../UI components/image/image";
import ExitButton from "./exitButton/exitButton";

function Header({isAuthorized, setIsAuthorized}) {

    const routerMap = {
        "/":0,
        "/scores":1,
        "/active_players":2,
        "/gameHistory": 3,
        "/players_list": 4
    }

    const currentPathName = window.location.pathname ;
    const currentPathIndex = routerMap[currentPathName] || 0;

    async function getScores() {
        const response = await fetch('/scores', {
            method: 'GET'
        });
        const data = await response.json();
        setScoresData(Object.values(data));
    }

    const checkAdmin = JSON.parse(localStorage.getItem("userData")).isAdmin;

    const [state, setState] = React.useState({activeItem: currentPathIndex});
    const [scoresData, setScoresData] = React.useState(null);

    if (currentPathIndex === 1 && !scoresData) {
        getScores()
    }

    const handleClick = (event, { index }) => {
        if (index === 1) {
            getScores()
        }
        setState({ activeItem: index });
    };


    async function deleteCookie() {
        await fetch('/auth/logout', {
            method: 'GET'
        })
    }

    return (

        <Router>

        <header>
            <div id="logo"><MyImage
                name="xoLogo"
            /></div>
            <div id="nav-panel">
                <Link
                    className={state.activeItem === 0 ? 'active' : ''}
                    onClick={(event) => handleClick(event, {index: 0})}
                    to="/">
                    Игровое поле
                </Link>

                <Link
                    className={state.activeItem === 1 ? 'active' : ''}
                    onClick={
                    (event) => {
                        handleClick(event, {index: 1})
                    }
                }
                    to="/scores">
                    Рейтинг
                </Link>

                <Link
                    className={state.activeItem === 2 ? 'active' : ''}
                    onClick={(event) => handleClick(event, {index: 2})}
                    to="/active_players">Активные игроки</Link>

                <Link
                    className={state.activeItem === 3 ? 'active' : ''}
                    onClick={(event) => handleClick(event, {index: 3})}
                    to="/game_history">
                    История игр
                </Link>

                {checkAdmin ? (
                    <>
                        <Link
                            className={state.activeItem === 4 ? 'active' : ''}
                            onClick={(event) => handleClick(event, {index: 4})}
                            to="/players_list">
                            Список игроков
                        </Link>
                    </>
                ) : null}

            </div>
            <ExitButton
                onClickFunction={
                async () => {
                    await deleteCookie();
                    localStorage.clear();
                    setIsAuthorized(false);
                }}
            />

        </header>
        <Routes>
            <Route path="/" element={<XOPanel/>}/>
            <Route path="/scores" element={
                <Scores
                    scoresData={scoresData}
            />
            }/>
            <Route path="/auth" element={<Auth/>}/>
            <Route path="/active_players" element={<ActivePlayers/>}/>
            <Route path="/game_history" element={<GameHistory/>}/>
            {checkAdmin ? (
                <>
            <Route path="/players_list" element={<PlayerList/>}/>
            </>
            ) : null}
        </Routes>

    </Router>
    )
}

export default Header;