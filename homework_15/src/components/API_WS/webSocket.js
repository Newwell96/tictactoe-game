import gameStore from "../store/GameStore";
import gameAPI from "./gameAPI";

class gameWS {
    socket;
    constructor() {
        this.socket = new WebSocket('ws://localhost:9000');
        this.socket.addEventListener("open", (event) => {
            console.log("connected to WS Server");
        })
        this.socket.addEventListener("message", (event) => {
            const data = JSON.parse(event.data);
            const message = data.message ? console.log(data.message, "--Ответ сервера--") : null;
            const field = data.field ? gameAPI.sendFieldState(data.field) : null;

            //TODO: Вызвать метод getChat и поменять состояние chat
            const chat = data.chat ? gameAPI.sendChatMessage(data.chat) : null;
            gameStore.initField();
            gameStore.initChat();
        });
    }

    sendData(field) {
        try {
            this.socket.send(JSON.stringify({field, action: "field" }));
        }
        catch (error) {
            this._handleError("Произошла ошибка отправки сообщения.")
        }
    }

    async sendMessage(userId, text, chat) {
        console.log(`${userId} отправил сообщение: ${text}`,"--Сообщение серверу--");
        try {
            this.socket.send(JSON.stringify({userId, text, chat, action: "message"}));
        }
        catch (error) {
            this._handleError("Произошла ошибка отправки сообщения.")
        }
    }

    async makeMove(userId, row, col, cell,field) {
        console.log(`Игрок ${userId} сделал ход ${row}:${col}`,"--Сообщение серверу--");
        try {
            this.socket.send(JSON.stringify({userId, row, col, cell, field, action: "move" }));
        }
        catch (error) {
            this._handleError(error, "Ошибка хода.")
        }
    }
}

const gameWebSocket = new gameWS();
export default gameWebSocket;